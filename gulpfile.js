var gulp = require("gulp");
var sass = require("gulp-sass");
var sourcemaps = require("gulp-sourcemaps");
var autoprefixer = require("gulp-autoprefixer");
var browserSync = require("browser-sync").create();
//var uglify = require("gulp-uglify");
var babel = require("gulp-babel");
//var concat = require("gulp-concat");

var paths = {
  pages: ["src/*.html"]
};

gulp.task("copy-html", function() {
  return gulp.src(paths.pages).pipe(gulp.dest("dist"));
});
gulp.task("html-wath", ["copy-html"], function(done) {
  browserSync.reload();
  done();
});

function handleError(error) {
  console.log(error.toString());
  this.emit("end");
}

gulp.task("js", function() {
  return gulp
    .src("./src/**/*.js")
    .pipe(sourcemaps.init())
     .pipe(
       babel({
         presets: ["@babel/env"]
       })
     )
    .on("error", handleError)
    // .pipe(uglify())
    // .pipe(concat("all.js"))
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest("./dist/"));
});

gulp.task("js-watch", ["js"], function(done) {
  browserSync.reload();
  done();
});

gulp.task("sass", function() {
  return gulp
    .src("src/**/*.scss")
    .pipe(sass({ outputStyle: "compressed" })) // Using gulp-sass
    .pipe(sourcemaps.init())
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false
      })
    )
    .pipe(gulp.dest("dist"));
});

gulp.task("sass-watch", ["sass"], function(done) {
  browserSync.reload();
  done();
});

gulp.task("run", ["copy-html", "sass", "js"], function() {
  browserSync.init({
    server: "dist"
  });

  gulp.watch("src/css/*.scss", ["sass-watch"]);
  gulp.watch("src/*.html", ["html-wath"]);
  gulp.watch("src/js/**/*.js", ["js-watch"]);
});
