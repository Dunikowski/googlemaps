$(document).ready(

    function() {

         const createLoader = function() {

            let script = document.createElement("script");
            script.type = "text/javascript";
            script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBx4zQvaksCJ8eXn3sBA6OByZYNuGVjbIc&callback=initMap";
            script.setAttribute("async", "");
            script.setAttribute("defer", "");
            document.body.appendChild(script);

          };

          createLoader();
        }
    );
          
    function initMap() {
        let GoogleMaps = {
            switchInfoWindow: false,
            option :[
              {
                "image": "images/tree.png",
                "image_h": "images/tree_hover.png",
                "position": {
                  "lat": 31.778649,
                  "lng": 35.243851
                },
                "title": "The Mount of Olives",
                "wiki_url": "https://en.wikipedia.org/wiki/Mount_of_Olives"
              },
              {
                "image": "images/city.png",
                "image_h": "images/city_hover.png",
                "position": {
                  "lat": 31.774331,
                  "lng": 35.23585
                },
                "title": "The City of David",
                "wiki_url": "https://en.wikipedia.org/wiki/City_of_David"
              },
              {
                "image": "images/amon.png",
                "image_h": "images/amon_hover.png",
                "position": {
                  "lat": 31.754298,
                  "lng": 35.236712
                },
                "title": "Armon Hanatziv",
                "wiki_url": "https://en.wikipedia.org/wiki/East_Talpiot"
              }
            ]
          };   
            GoogleMaps.jsMap = document.getElementById("map");
          
            let map = new google.maps.Map(GoogleMaps.jsMap, {
              scrollwheel: false,
              center: {
                lat: GoogleMaps.option[1].position.lat,
                lng: GoogleMaps.option[1].position.lng
              },
              zoom: 14
            });
          
            const change_icon = () => {
              const _gm_icon = $(GoogleMaps.jsMap).find(".gm-icon");
          
              _gm_icon.each(function () {
                let url_img = $(this)
                  .attr("src")
                  .replace("_hover.png", ".png");
          
                $(this).attr("src", url_img);
              });
            };
          
            for (let i = 0; i < GoogleMaps.option.length; i++) {
              
              const contentString = `
              <div class="info-window-content">
                <div class="wrap-gm-icon">
                  <img class="gm-icon" src='${GoogleMaps.option[i].image}' alt="icon"/>
                </div>
                <div class="info-window-maps">
                  <h3 class="title-map">${GoogleMaps.option[i].title}</h3>
                  <a class="button-go-to" href="${GoogleMaps.option[i].wiki_url}">CLICK FOR DIRECTIONS</a>
                </div>
              <div>
              `;
          
              const infowindow = new google.maps.InfoWindow({
                content: contentString,
                pixelOffset: new google.maps.Size(11, 166)
              });
          
              infowindow.setPosition({
                lat: GoogleMaps.option[i].position.lat,
                lng: GoogleMaps.option[i].position.lng
              });
          
              if (i == 1) {
                infowindow.setZIndex(2);
              } else {
                infowindow.setZIndex(1);
              }
          
              infowindow.open(map);
          
              google.maps.event.addListener(infowindow, "domready", function() {
                
                const _gbutton_go_to = $(GoogleMaps.jsMap).find(".button-go-to");
                const _gm_style_iw = _gbutton_go_to.closest(".gm-style-iw");
                _gm_style_iw.closest('.gm-style-iw-a').parent().addClass("js-wrap-iw");
                _gm_style_iw.prev().css({
                  display: "none"
                });
                _gbutton_go_to.click((e) => e.stopPropagation());
                _gm_style_iw.find(".gm-icon").on('mouseover click', e => {
                  e.stopPropagation();
                  const $this = $(e.target);
                  let url_img;
                  $this.closest('.js-wrap-iw').siblings().removeClass("active");
                  change_icon();
                  GoogleMaps.switchInfoWindow = true;
                  $this.closest(".js-wrap-iw").addClass("active");
          
                  if (!/_hover.png/.test($this.attr("src"))) {
                    url_img = $this.attr("src").replace(".png", "_hover.png");
                  }
          
                  $this.attr("src", url_img);
                });
                
                _gm_style_iw.mouseleave(e => {
                  e.stopPropagation();
                  GoogleMaps.switchInfoWindow = false;
                  google.maps.event.trigger(map, "click");
                });
          
                _gm_style_iw.mousemove(e => {
                  if (!GoogleMaps.switchInfoWindow) return;
                  const relX =
                    e.pageX -
                    $(e.target)
                      .closest(".gm-style-iw")
                      .offset().left;
                  const relY =
                    e.pageY -
                    $(e.target)
                      .closest(".gm-style-iw")
                      .offset().top;
          
                  if (relY < 70) {
                    if (relX < 40 || relX > 120) {
                      GoogleMaps.switchInfoWindow = false;
                      google.maps.event.trigger(map, "click");
                    }
                  }
                });
              });
            }

            google.maps.event.addListener(map, "click", function() {
              const _js_wrap_iw = $(GoogleMaps.jsMap).find(".js-wrap-iw");
              if (_js_wrap_iw.length > 0) {
                _js_wrap_iw.removeClass('active');
                change_icon();
              }
              return;
            });
          };
